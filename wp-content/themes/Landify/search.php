<?php get_header(); ?>
<div class="all">
    <div class="jose">
        <?php if (have_posts()) : ?>
            <h2 class="pagetitle">Search Results for "<?php echo $s ?>"</h2>
            <?php while (have_posts()) : the_post(); ?>
                <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>">
                <div class="all" id="post-<?php the_ID(); ?>">
                    <h2><?php the_title(); ?></h2>
                    <?php
                    if ( function_exists('the_excerpt') && is_search() ) {
                        the_excerpt();
                    } ?>
                    <p>
                        <?php the_time('F jS, Y') ?> 
                    </p>
                </div>
                </a>
            <?php endwhile; ?>
            <div class="navigation">
                <div class="alignleft"><?php next_posts_link('&laquo; Previous') ?></div>
                <div class="alignright"><?php previous_posts_link('Next &raquo;') ?></div>
            </div>
        <?php else : ?>
            <h2 class="center">No posts found. Try a different search?</h2>
            <?php include (TEMPLATEPATH . '/searchform.php'); ?>
        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>