<footer>
<div class="all">
	<div class="jose">
		<div class="col">
			<div class="all">
				<img src="<?php echo site_url();?>/wp-content/uploads/2021/06/landify-logo.png">
			</div>
			<div class="all paddtop">
				<?php if(is_active_sidebar('footer_menu')){  dynamic_sidebar('footer_menu'); }?>
			</div>
			<div class="all paddtop">
				<p> &#169; 2020 Landify UI Kit. All Rights Reserved</p>
			</div>
		</div>
		<div class="col">
			<strong>Get the app</strong>
			<div class="all paddtop"><a href="#" ><img src="<?php echo site_url();?>/wp-content/uploads/2021/06/appstore.png"></a></div>
			<div class="all paddtop"><a href="#" ><img src="<?php echo site_url();?>/wp-content/uploads/2021/06/playstore.png"></a></div>
		</div>
	</div>
</div>
</footer>
</body>
<?php wp_footer();?>