<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes() ?>>
<head>
<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen,projection" />
<meta name="viewport" content="width=device-width; initial-scale = 1.0; 
maximum-scale=1.0; user-scalable=no" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<?php wp_head();?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
</head>
<body <?php body_class();?>>
<header>
   	<div class="jose">
   			<div class="logo">
   				<a href="<?php echo site_url();?>/">
	    			<img src="<?php echo site_url();?>/wp-content/uploads/2021/06/landify-logo.png">
	    		</a>
   			</div>
   			<div class="navigation-menu">
               <div class="all">
                  <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'container_id' => 'cssmenu',  'theme_location' => 'primary' ) ); ?>
               </div>
   			</div>
			<div class="downloadables">
					<div class="col"><a href="#" ><img src="<?php echo site_url();?>/wp-content/uploads/2021/06/appstore.png"></a></div>
					<div class="col"><a href="#" ><img src="<?php echo site_url();?>/wp-content/uploads/2021/06/playstore.png"></a></div>
			</div>
   	</div>
</header>