$(document).ready(function() {
    var maxHeight = 0;
    $(".news-txt").each(function() {
        if ($(this).height() > maxHeight) {
            maxHeight = Math.max(maxHeight, $(this).height());
        }
    });
    $(".news-txt").css({ height: maxHeight + 50 + 'px' });
});


// Beginning of fancybox js
$(document).ready(function() {
    $('.fancybox').fancybox({
        padding: 0,
        maxWidth: '100%',
        maxHeight: '100%',
        width: 560,
        height: 315,
        autoSize: true,
        closeClick: true,
        openEffect: 'elastic',
        closeEffect: 'elastic'
    });
});
// End of fancybox js

$(document).ready(function() {
    $('.flexslider').flexslider({
        animation: "fade",
        slideshowSpeed: 6000,
        animationSpeed: 2000,
        controlNav: true,
        pauseOnAction: true,
        controlNav: false,
        directionNav: true
    });
})

$(document).ready(function() {
    $('.maps-flexslider').flexslider({
        animation: "slide",
        slideshowSpeed: 10000,
        animationSpeed: 2000,
        controlNav: true,
        pauseOnAction: true,
        controlNav: true,
        directionNav: false
    });
})

//Js for Carousel- Start
jQuery(document).ready(function() {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        items: 4,
        loop: true,
        margin: 20,
        autoPlay: true,
        autoPlayTimeout: 1000,
        autoPlayHoverPause: true,
        navigation: false,
        pagination: true,
    });
    jQuery('.play').on('click', function() {
        owl.trigger('autoPlay.play.owl', [10])
    });
    jQuery('.stop').on('click', function() {
        owl.trigger('autoPlay.stop.owl')
    });
    $(".owl-prev").html('<i class="fa fa-chevron-left"></i>');
    $(".owl-next").html('<i class="fa fa-chevron-right"></i>');
});
//Js for Carousel- End

// js for Tabs
// tabbed content
$(".tab_content").hide();
$(".tab_content:first").show();

/* if in tab mode */
$("ul.tabs li").click(function() {

    $(".tab_content").hide();
    var activeTab = $(this).attr("rel");
    $("#" + activeTab).fadeIn();

    $("ul.tabs li").removeClass("active");
    $(this).addClass("active");

    $(".tab_drawer_heading").removeClass("d_active");
    $(".tab_drawer_heading[rel^='" + activeTab + "']").addClass("d_active");

});
/* if in drawer mode */
$(".tab_drawer_heading").click(function() {

    $(".tab_content").hide();
    var d_activeTab = $(this).attr("rel");
    $("#" + d_activeTab).fadeIn();

    $(".tab_drawer_heading").removeClass("d_active");
    $(this).addClass("d_active");

    $("ul.tabs li").removeClass("active");
    $("ul.tabs li[rel^='" + d_activeTab + "']").addClass("active");
});


/* Extra class "tab_last" 
   to add border to right side
   of last tab */
$('ul.tabs li').last().addClass("tab_last");

$('.counter').each(function() {
    var $this = $(this),
        countTo = $this.attr('data-count');

    $({
        countNum: $this.text()
    }).animate({
            countNum: countTo
        },

        {
            duration: 5000,
            easing: 'linear',
            step: function() {
                $this.text(commaSeparateNumber(Math.floor(this.countNum)));
            },
            complete: function() {
                $this.text(commaSeparateNumber(this.countNum));
                //alert('finished');
            }
        }
    );
});

function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}

//js code for sticky top header-start
jQuery(document).ready(function($) {
    //you can now use $ as your jQuery object.
    var body = $('body');
    $(window).on("scroll", function() {
        if ($(window).scrollTop() > 10) {
            $("header").addClass("active");
        } else {
            //remove the background property so it comes transparent again (defined in your css)
            $("header").removeClass("active");
        }
    });
});
//js code for sticky top header-end


$(".attachment-full").each(function() {
    $(this).wrap($('<a/>', {
        href: $(this).attr('src')
    }).attr('data-fancybox', 'gallery'));
});


'use strict';

(function() {
    // Specify the deadline date
    var deadlineDate = new Date('May 29, 2021 23:59:59').getTime();

    // Cache all countdown boxes into consts
    var countdownDays = document.querySelector('.countdown__days .number');
    var countdownHours = document.querySelector('.countdown__hours .number');
    var countdownMinutes = document.querySelector('.countdown__minutes .number');
    var countdownSeconds = document.querySelector('.countdown__seconds .number');

    // Update the count down every 1 second (1000 milliseconds)
    setInterval(function() {
        // Get current date and time
        var currentDate = new Date().getTime();

        // Calculate the distance between current date and time and the deadline date and time
        var distance = deadlineDate - currentDate;

        // Calculations the data for remaining days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor(distance % (1000 * 60 * 60 * 24) / (1000 * 60 * 60));
        var minutes = Math.floor(distance % (1000 * 60 * 60) / (1000 * 60));
        var seconds = Math.floor(distance % (1000 * 60) / 1000);

        // Insert the result data into individual countdown boxes
        countdownDays.innerHTML = days;
        countdownHours.innerHTML = hours;
        countdownMinutes.innerHTML = minutes;
        countdownSeconds.innerHTML = seconds;
    }, 1000);
})();
//End of JS for countdown


// js for Tabs
// tabbed content
// http://www.entheosweb.com/tutorials/css/tabs.asp
$(".tab_content").hide();
$(".tab_content:first").show();

/* if in tab mode */
$("ul.tabs li").click(function() {

    $(".tab_content").hide();
    var activeTab = $(this).attr("rel");
    $("#" + activeTab).fadeIn();

    $("ul.tabs li").removeClass("active");
    $(this).addClass("active");

    $(".tab_drawer_heading").removeClass("d_active");
    $(".tab_drawer_heading[rel^='" + activeTab + "']").addClass("d_active");

});
/* if in drawer mode */
$(".tab_drawer_heading").click(function() {

    $(".tab_content").hide();
    var d_activeTab = $(this).attr("rel");
    $("#" + d_activeTab).fadeIn();

    $(".tab_drawer_heading").removeClass("d_active");
    $(this).addClass("d_active");

    $("ul.tabs li").removeClass("active");
    $("ul.tabs li[rel^='" + d_activeTab + "']").addClass("active");
});


/* Extra class "tab_last" 
   to add border to right side
   of last tab */
$('ul.tabs li').last().addClass("tab_last");



(function() {
    // Slide In Panel - by CodyHouse.co
    var panelTriggers = document.getElementsByClassName('js-cd-panel-trigger');
    if (panelTriggers.length > 0) {
        for (var i = 0; i < panelTriggers.length; i++) {
            (function(i) {
                var panelClass = 'js-cd-panel-' + panelTriggers[i].getAttribute('data-panel'),
                    panel = document.getElementsByClassName(panelClass)[0];
                // open panel when clicking on trigger btn
                panelTriggers[i].addEventListener('click', function(event) {
                    event.preventDefault();
                    addClass(panel, 'cd-panel--is-visible');
                });
                //close panel when clicking on 'x' or outside the panel
                panel.addEventListener('click', function(event) {
                    if (hasClass(event.target, 'js-cd-close') || hasClass(event.target, panelClass)) {
                        event.preventDefault();
                        removeClass(panel, 'cd-panel--is-visible');
                    }
                });
            })(i);
        }
    }
    //class manipulations - needed if classList is not supported
    //https://jaketrent.com/post/addremove-classes-raw-javascript/
    function hasClass(el, className) {
        if (el.classList) return el.classList.contains(className);
        else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
    }

    function addClass(el, className) {
        if (el.classList) el.classList.add(className);
        else if (!hasClass(el, className)) el.className += " " + className;
    }

    function removeClass(el, className) {
        if (el.classList) el.classList.remove(className);
        else if (hasClass(el, className)) {
            var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
            el.className = el.className.replace(reg, ' ');
        }
    }
})();




// Beginning of fancybox js
$(document).ready(function() {
    $('.fancybox').fancybox({
        padding: 0,
        maxWidth: '100%',
        maxHeight: '100%',
        width: 560,
        height: 315,
        autoSize: true,
        closeClick: true,
        openEffect: 'elastic',
        closeEffect: 'elastic'
    });
});
// End of fancybox js