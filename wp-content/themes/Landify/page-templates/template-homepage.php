<?php /* Template Name: Homepage Template */ ?>
<?php get_header(); ?>
<div class="all masthead">
	<img src="<?php echo site_url();?>/wp-content/uploads/2021/06/hero-bg.png" alt="Landify">
	<div class="caption">
		<div class="col wow fadeInLeft"  data-wow-delay="0.3s">
			<div class="container py-5">
				<div class="output" id="output">
					<h1 class="cursor"></h1>
				</div>
			</div>
			<div class="all button">
				<a href="#">Get Started</a>
			</div>
		</div>
		<div class="col wow fadeInRight"  data-wow-delay="0.3s">
			<img src="<?php the_field('masthead_image');?>" alt="Landify">
		</div>

	</div>
</div>
<div class="all partners wow bounceInRight"  data-wow-delay="0.3s">
	<img src="<?php the_field('affiliates_image');?>" alt="Landify">
</div>
<div class="all wow fadeInDown"  data-wow-delay="0.5s">
	<div class="jose">
		<div class="all center">
			<div class="inner">
				<?php the_field('features_title');?>
			</div>
		</div>
				<?php
						$i = 0;
						$args = array(
							'showposts' => '-1',
							'post_type' => 'features',
							'order_by' => 'date', 
							'order'=> 'Asc', 
						);
						query_posts( $args );
						while ( have_posts() ) : the_post(); ?>
					<?php
					if($i == 0) {
						echo '<div class="all features">';
					}
					?>
						<div class="col">
								<?php the_post_thumbnail(); ?>
								<h3><?php the_title(); ?>	</h3>
								<?php the_content(); ?>	
						</div>
					<?php
					$i++;
					if($i == 3) {
						$i = 0;
						echo '</div>';
					}
					?>
					<?php endwhile; ?>
					<?php
					if($i > 0) {
						echo '</div>';
					}
					?>
					<?php wp_reset_query();?>
    </div>
	</div>
</div>
<div class="all headline">
	<div class="jose">
		<div class="col">
			<img src="<?php the_field('headline_image');?>" alt="Landify">
		</div>
		<div class="col">
			<?php the_field('headline_title_descripion');?>
			<div class="all button">
				<a href="#">Get Started  <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
</div>
<div class="all summary">
	<div class="jose">
		<div class="col">
			<?php the_field('fashion_section_column_1');?>
		</div>
		<div class="col">
			<?php the_field('fashion_section_column_2');?>
		</div>
	</div>
</div>
<div class="all cover-image">
	<div class="jose">
		<img src="<?php the_field('bottom_cover_image');?>" alt="Landify">
	</div>
</div>
<?php get_footer(); ?>