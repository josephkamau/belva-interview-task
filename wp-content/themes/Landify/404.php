<?php get_header(); ?>
<div class="all more-about paddbottom">
	<div class="jose">
		<div class="half">
			<img src="<?php echo site_url();?>/wp-content/uploads/2019/03/error-page.jpg">
		</div>
		<div class="half">
			<h3>OOPS...</h3>
	        <p>Something went wrong — the page you are looking for doesn't exist.</p>
	        <p>Maybe you mistyped the URL?</p>
		</div>
	</div>
</div>
<?php get_footer(); ?>