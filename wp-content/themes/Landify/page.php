<?php get_header();?>
<div class="all breadcrumbs">
    <div class="jose">
      <?php
        if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb('
        <p id="breadcrumbs">','</p>
        ');
        }
      ?>
    </div>
</div>
<div class="all paddtop">
	<div class="jose">
		 <?php if ( have_posts() ) : while ( have_posts() ) : the_post();       
  the_content(); // displays whatever you wrote in the wordpress editor
  endwhile; endif; //ends the loop
 ?>
	</div>
</div>
<?php get_footer();?>