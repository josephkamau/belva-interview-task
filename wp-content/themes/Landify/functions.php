<?php
/**
 * Theme functions and definitions
 */
/**
 * First, let's set the maximum content width based on the theme's design and stylesheet.
 * This will limit the width of all uploaded images and embeds.
 */
if ( ! isset( $content_width ) )
    $content_width = 800; /* pixels */
 
if ( ! function_exists( 'theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function theme_setup() {
 
    /**
     * Make theme available for translation.
     * Translations can be placed in the /languages/ directory.
     */
    load_theme_textdomain( 'theme', get_template_directory() . '/languages' );
 
    /**
     * Add default posts and comments RSS feed links to <head>.
     */
    add_theme_support( 'automatic-feed-links' );
 
    /**
     * Enable support for post thumbnails and featured images.
     */
    add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );
 
    /**
     * Add support for two custom navigation menus.
     */
    register_nav_menus( array(
        'primary'   => __( 'Primary Menu', 'theme' ),
        'secondary' => __('Secondary Menu', 'theme' )
    ) );
 
    /**
     * Enable support for the following post formats:
     * aside, gallery, quote, image, and video
     */
    add_theme_support( 'post-formats', array ( 'aside', 'gallery', 'quote', 'image', '  ' ) );
}
endif; // theme_setup
add_action( 'after_setup_theme', 'theme_setup' );

add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

function disable_wp_emojicons() {

  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}
add_theme_support('woocommerce');

// CUSTOM EXCERT WORD LIMITER
function limit_words($string, $word_limit) {

  // creates an array of words from $string (this will be our excerpt)
  // explode divides the excerpt up by using a space character

  $words = explode(' ', $string);

  // this next bit chops the $words array and sticks it back together
  // starting at the first word '0' and ending at the $word_limit
  // the $word_limit which is passed in the function will be the number
  // of words we want to use
  // implode glues the chopped up array back together using a space character

  return implode(' ', array_slice($words, 0, $word_limit));

}
//----------------------------------------------
//--------------add theme support for thumbnails
//----------------------------------------------
if ( function_exists( 'add_theme_support')){
  add_theme_support( 'post-thumbnails' );
}
add_image_size( 'admin-list-thumb', 80, 80, true); //admin thumbnail preview
add_image_size( 'album-grid', 450, 450, true );
//----------------------------------------------
//------------------------------------enqueue scripts
//----------------------------------------------
function jss_load_scripts(){

  wp_enqueue_style( 'style', get_stylesheet_uri() );
  wp_enqueue_style( 'jose-style', get_template_directory_uri() . '/assets/css/jose-style.css', array(), '1.1', 'all');
  wp_enqueue_style( 'menu', get_template_directory_uri() . '/assets/css/menu.css', array(), '2.1', 'all');
  wp_enqueue_style( 'flexslider', get_template_directory_uri() . '/assets/css/flexslider.css', array(), '2.1', 'all');
  wp_enqueue_style( 'carousel;', get_template_directory_uri() . '/assets/css/owl.carousel.css', array(), '2.1', 'all');
  wp_enqueue_style( 'flexslider-min', get_template_directory_uri() . '/assets/css/flexslider.min.css', array(), '1.1', 'all');
  wp_enqueue_style( 'owl-min', get_template_directory_uri() . '/assets/css/owl.carousel.min.css', array(), '1.1', 'all');

  //deregister for google jQuery cdn
  wp_deregister_script( 'jquery' );
  wp_register_script( 'jquery', "http" . ( $_SERVER['SERVER_PORT'] == 443 ? "s" : "" ) . "://code.jquery.com/jquery-latest.min.js",  array(), true, true );
  wp_enqueue_script( 'jquery' );
  

  wp_register_script( 'flex-slider-js', 'https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.0/jquery.flexslider-min.js',  array(), false, true );
  wp_enqueue_script( 'flex-slider-js' );
  wp_enqueue_script('carousel-js', get_template_directory_uri().'/assets/js/owl.carousel.min.js', array( 'jquery' ), '1.0', true);
  wp_enqueue_script('accordion-js', get_template_directory_uri().'/assets/js/accordion.min.js', array( 'jquery' ), '1.0', true);
  //nav js
  wp_register_script( 'nav', get_template_directory_uri() . '/assets/js/nav.js',  array(), true, true );
  wp_enqueue_script( 'nav' );
  wp_register_script( 'wow-script', get_template_directory_uri() . '/assets/js/wow.js',  array(), true, true );
  wp_enqueue_script( 'wow-script' );

  wp_register_script( 'flexslider-script', get_template_directory_uri() . '/assets/js/flexslider.js',  array(), true, true );
  wp_enqueue_script( 'flexslider-script' );

  wp_register_style( 'fancybox_css', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.css' );
  wp_enqueue_style('fancybox_css');

  wp_register_script( 'fancybox_js', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.js', null, null, true );
  wp_enqueue_script('fancybox_js');
	
}
//set it off
add_action( 'wp_enqueue_scripts', 'jss_load_scripts' );


// Regsiter Footer widget area- start

if (function_exists('register_sidebar')) {
  register_sidebar(array(
  'name' => 'Social Media Links',
  'id'   => 'socials',
  'description'   => 'Display Widget Items in the Footer.',
  'before_widget' => '<div class="footer-widget col-lg-4 col-md-4 col-sm-4 col-xs-12">',
  'after_widget'  => '</div>',
  'before_title'  => '<h4>',  
  'after_title'   => '</h4>'
      ));
}

if (function_exists('register_sidebar')) {
  register_sidebar(array(
  'name' => 'Footer Menu',
  'id'   => 'footer_menu',
  'description'   => 'Display Widget Items in the Footer.',
  'before_widget' => '<div class="footer-widget col-lg-4 col-md-4 col-sm-4 col-xs-12">',
  'after_widget'  => '</div>',
  'before_title'  => '<h4>', 
  'after_title'   => '</h4>'
      ));
}

if (function_exists('register_sidebar')) {
  register_sidebar(array(
  'name' => 'Copyright',
  'id'   => 'copyright',
  'description'   => 'Display Widget Items in the Footer.',
  'before_widget' => '<div class="footer-widget col-lg-4 col-md-4 col-sm-4 col-xs-12">',
  'after_widget'  => '</div>',
  'before_title'  => '<h4>', 
  'after_title'   => '</h4>'
      ));
}
// Regsiter Footer widget area- end


//execute php in widgets
function php_execute($html){
if(strpos($html,"<"."?php")!==false){ ob_start(); eval("?".">".$html);
$html=ob_get_contents();
ob_end_clean();
}
return $html;
}
add_filter('widget_text','php_execute',100);

//Clean up wordpress header
    remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
    remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
    remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
    remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
    remove_action( 'wp_head', 'index_rel_link' ); // index link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
    remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
    remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );


function register_features_cpt(){
    register_post_type('features', array(
        'labels'            =>  array(
            'name'          =>      __('Tailor-made Features'),
            'singular_name' =>      __('Tailor-made Features'),
            'all_items'     =>      __('View Tailor-made Features'),
            'add_new'       =>      __('New Tailor-made Features'),
            'add_new_item'  =>      __('New Tailor-made Features'),
            'edit_item'     =>      __('Edit Tailor-made Features'),
            'view_item'     =>      __('View Tailor-made Features'),
            'search_items'  =>      __('Search For Tailor-made Features'),
            'no_found'      =>      __('No Tailor-made Features'),
            'not_found_in_trash' => __('No Tailor-made Features in Trash')
        ),
        'public'            =>  true,
        'publicly_queryable'=>  true,
        'show_ui'           =>  true,
        'query_var'         =>  true,
        'show_in_nav_menus' =>  false,
        'show_in_nav_menus' =>  true,
        'capability_type'   =>  'post',
        'hierarchical'      =>  false,
        'rewrite'           =>  array('slug' => 'features', 'with_front' => true),
        'menu_position'     =>  21,
        'supports'          =>  array('title','editor', 'thumbnail'),
        'has_archive'       =>  true
    ));
}
add_action('init', 'register_features_cpt');

function custom_pagination($numpages = '', $pagerange = '', $paged='') {

  if (empty($pagerange)) {
    $pagerange = 2;
  }

  /**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   * 
   * It's good because we can now override default pagination
   * in our theme, and use this function in default quries
   * and custom queries.
   */
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }
  /** 
   * We construct the pagination arguments to enter into our paginate_links
   * function. 
   */
  $pagination_args = array(
    'base'            => get_pagenum_link(1) . '%_%',
    'format'          => 'page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('&laquo;'),
    'next_text'       => __('&raquo;'),
    'type'            => 'plain',
    'add_args'        => false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
    echo "<nav class='custom-pagination'>";
      // echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
      echo $paginate_links;
    echo "</nav>";
  }
}