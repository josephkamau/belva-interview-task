<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'landify' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '1vk>3?q*#8skYl5n4m9X^a>ZQd#sH(bM {bzV;sQW@QX,!eIKOD;SJi!!^te-q:p' );
define( 'SECURE_AUTH_KEY',  'ee.YZYOE4UO;EFO]w}|C_eoF%GJpBT*L|sLAn9uv50f4`@Pwz*8kCF,{|!ykcf$1' );
define( 'LOGGED_IN_KEY',    'qHay1JaZ}g>Jyc}m#K~BJ]b5*:OA,L/;YI>k[s.{BD8+f=xH[Rvdq4twk_WdFLhf' );
define( 'NONCE_KEY',        'Lr*u<~nq^m/RG-1I.FtiXN!pF>5f2&FZM ,PYp[Ms37oZuneQ*b fyhI!BS2W5b5' );
define( 'AUTH_SALT',        '*,LR2wkBilo}6m7{:zoH&8bVKe}*}pvYK-k?WcI63%Edf;T6!{?Ro4mh;<%%MdF%' );
define( 'SECURE_AUTH_SALT', '_lL/fA`<#Xd<$@^{6AJuIPcJJvtD(w!tvD{zspjaC5a@I@8H,-n>e<]/Gz8$n>Xa' );
define( 'LOGGED_IN_SALT',   'od_))(nI:X3tm2cSEZ.A+DH}1N6=/H~BKRgsB6h tb-yF.%zPlO+D&I/-_HEItL&' );
define( 'NONCE_SALT',       'x2UJ8e/yG{~+$E}w.0?MU$Nh6#~/u}_Td}b+1{A7mlI/m+ aO]O[I?|IU}SF%:eA' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
define('FS_METHOD', 'direct');